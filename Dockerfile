FROM python

RUN pip install lizard --break-system-packages

# limits after which lizard will raise a warning
ENV MAX_FUNCTION_LENGTH=200
ENV MAX_PARAMETER_COUNT=5
ENV MAX_COMPLEXITY=5

WORKDIR /project

CMD ["sh", "-c", "lizard -Eduplicate --length $MAX_FUNCTION_LENGTH --CCN $MAX_COMPLEXITY -a $MAX_PARAMETER_COUNT"]
