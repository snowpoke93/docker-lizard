# Lizard Docker Container

## Customize

You can adjust some settings by setting the following environment variables:
- `MAX_FUNCTION_LENGTH`: Maximal allowed lines of code in a function. *(default: 200)*
- `MAX_PARAMETER_COUNT`: Number of maximal allowed parameters for a function. *(default: 5)*
- `MAX_COMPLEXITY`: Maximal allowed Cyclomatic Complexity for a function. *(default: 5)*
